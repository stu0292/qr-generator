// package main creates a qr code from 
package main

import (
	"image/png"
	"os"

	"gitlab.com/stu-b-doo/cli"
	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
)

const usage = "qr {message} {output.png}"

func main() {

	// cli args
	cli.Parse(usage)
	message := cli.Marg(0, "Message to encode required")
	outfile := cli.Marg(1, "Output filepath required (png)")

	// Create the barcode
	qrCode, _ := qr.Encode(message, qr.M, qr.Auto)

	// Scale the barcode to 200x200 pixels
	qrCode, _ = barcode.Scale(qrCode, 200, 200)

	// create the output file
	file, _ := os.Create(outfile)
	defer file.Close()

	// encode the barcode as png
	png.Encode(file, qrCode)
}
